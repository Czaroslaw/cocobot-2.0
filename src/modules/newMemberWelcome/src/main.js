const { Events } = require('discord.js');
const lang = require(global.appRoot + '/src/i18n/en.json');
const i18n = require(global.appRoot + '/src/shared/i18nHelper.js');
const settings = require('../settings.json').settings;

global.client.on(Events.GuildMemberAdd, event => {
    event.guild.channels.cache.get(settings.welcomeChannelID).send(i18n.t(lang.Event.NewMember, {
        'name': event.user,
        'ruleChannel': `<#${settings.ruleChannelID}>`,
        'roleChannel': `<#${settings.roleChannelID}>`
    }));
});