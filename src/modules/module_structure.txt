A module's ought to look like this:
example_module/
├─ src/
│  ├─ other_folders/
│  ├─ main.js
├─ module.json
├─ settings.json
Note that the commands, events, shared subfolders are a merely recommended structure. The bare bones for a module working is the existence of <module>/src/main.js

The module.json file's contents should look something like this:
{
    "name": "Example Module",
    "version": "0.1"
}