const fs = require('fs');
const lang = require('../../src/i18n/en.json');
const i18n = require('../../src/shared/i18nHelper.js');
const fileHelper = require('../shared/fileHelper.js');
const configDirPath = './config/';

if (fs.existsSync(configDirPath + 'mainConfig.json')) {
    let { main: configFile } = require('../.' + configDirPath + 'mainConfig.json');
    if (configFile.token == "") {
        throw i18n.t(lang.Runtime.EmptyToken);
    }
} else {
    fileHelper.generateConfigFile(configDirPath + 'mainConfig.json', {
        main: {
            token: "",
            derpiKey: ""
        }
    });
    throw i18n.t(lang.Runtime.NoConfigFile);
}

if (!fs.existsSync(configDirPath + 'settings.json')) {
    fileHelper.generateConfigFile(configDirPath + 'settings.json', require('../.' + configDirPath + 'defaultSettings.json'));
    throw i18n.t(lang.Runtime.NoSettingsFile);
}