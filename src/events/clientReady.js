const { Events } = require('discord.js');
const lang = require(global.appRoot + '/src/i18n/en.json');
const i18n = require(global.appRoot + '/src/shared/i18nHelper.js');

global.client.once(Events.ClientReady, client => {
    console.log("\n" + i18n.t(lang.Runtime.Ready, { 'name': client.user.tag }));
});