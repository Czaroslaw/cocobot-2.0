const tagRegex = /{(\w+)}/g;

/**
 * @param {Object} key //check if it actually is object
 * @param {Object} params
 */
function t(key, params = null) {
    if (params !== null) {
        if (typeof(params) !== 'object') {
            throw 'Parameter is not an object!'
        }
    }

    let tags = searchForTags(key);
    let output = key;

    if (params !== null) {
        for (let i = 0; i <= tags.length; i++) {
            output = output.replace(`\{${tags[i]}\}`, params[tags[i]]);
        }
    }

    return output;
}

function searchForTags(input) {
    try {
        let output = [];
        let tagsFound = [...input.matchAll(tagRegex)];
        tagsFound.forEach((element, index) => {
            output[index] = element[1];
        });
        return output;
    } catch (err) {
        return 'undefined';
    }
}

module.exports = {
    tagRegex,
    t,
}