const fs = require('fs');

/**
 * @param {string} path
 * @param {Object} structure
 */
function generateConfigFile(path, structure) {
    fs.writeFileSync(path, JSON.stringify(structure, null, 4));
}

/**
 * @param {string} path
 */
function folderToArray(path) {
    return array = fs.readdirSync(path).filter(function(file) {
        return fs.statSync(path + '/' + file).isDirectory();
    });
}

/**
 * @param {string} path
 */
function isModuleStructureCorrect(path) {
    try {
        let moduleInfo = require(path);
        if (!(moduleInfo.name && moduleInfo.version)) {
            console.error('Could not load module' + path + ' - file contains no name or version info.');
            return false;
        }
        return true;
    } catch (err) {
        console.error(err);
        return false;
    }
}

/**
 * @param {string} path
 */
function createFile(path, contents) {
    try {
        if (fs.existsSync(path)) {
            throw 'File already exists!';
        }
        fs.appendFile(path, contents, function(err) {
            if (err) {
                throw err;
            } else {
                console.log(`File ${path} has been created successfully.`);
            }
        });
    } catch (err) {
        console.log(`Could not create file: ${err}`)
    }
}

module.exports = {
    generateConfigFile,
    folderToArray,
    isModuleStructureCorrect,
    createFile,
}