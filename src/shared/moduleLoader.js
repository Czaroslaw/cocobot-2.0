const moduleDir = global.appRoot + '/src/modules';
const fileHelper = require('./fileHelper.js');

let folders = fileHelper.folderToArray(moduleDir);
global.loadedModules = [];
folders.forEach(folder => {
    if (fileHelper.isModuleStructureCorrect(moduleDir + '/' + folder + '/module.json')) {
        let moduleInfo = require(moduleDir + '/' + folder + '/module.json');
        if (global.loadedModules.find(module => module.name == moduleInfo.name)) {
            console.log(moduleInfo.name + ' v' + moduleInfo.version + ': A module with the same name found. Ignored.');
            //TODO: load one with the newer version. If the versions are the same, simply ignore.
        } else {
            global.loadedModules.push(require(moduleDir + '/' + folder + '/module.json'));
            try {
                require(moduleDir + '/' + folder + '/src/main.js');
                console.log(moduleInfo.name + ' v' + moduleInfo.version + ' has been loaded successfuly.');
            } catch (err) {
                console.log('Could not load module: ' + err);
            }
        }
    }
})