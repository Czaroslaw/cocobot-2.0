const lang = require('./i18n/en.json');
const i18n = require('./shared/i18nHelper.js');
const fileHelper = require('./shared/fileHelper.js');
const args = process.argv.filter((e, index) => { return index > 1 });
const fs = require('fs');
const defaultSettingStructure = { enabled: "true", settings: {}, }

if (args.length > 0 && args[0] !== "help") {
    switch (args[0]) {
        case 'create':
            generateModuleStructure(args[1]);
            break;
        default:
            console.log(i18n.t(lang.ModuleGenerator.UnknownCommand));
            break;
    }
} else {
    console.log(i18n.t(lang.ModuleGenerator.Help));
}

/*//TODO:
>make sure that the folder does not exist already - DONE
>generate settings and module jsons
>create an empty src/main.js file
*/

function generateModuleStructure(module_name) {
    if (fs.existsSync('./src/modules/' + module_name)) {
        throw 'Directory already exists!';
    }
    fs.mkdirSync(`./src/modules/${module_name}/`);
    fs.mkdirSync(`./src/modules/${module_name}/src`);
    fileHelper.createFile(`./src/modules/${module_name}/src/main.js`, '');
    fileHelper.generateConfigFile(`./src/modules/${module_name}/settings.json`, defaultSettingStructure);
    fileHelper.generateConfigFile(`./src/modules/${module_name}/module.json`, { "name": module_name, "version": "1.0" });
}