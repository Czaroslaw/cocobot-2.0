
# CocoBot 2.0
This project is a ground-up rewrite of my first Discord bot, which incidentally was also my very first NodeJS project.

The purpose of this refactor is to make a Discord API bot framework which could be easily expanded without requiring lots of dependencies (other than Discord.js, which is crucial for the application to work at all).

## Features 

- Core modules:
     - Welcome message
- Module structure generator

## Planned features 

- Support for multiple languages based on the language chosen in the settings.json file
- Support for database connections
- Derpibooru image search as a core module
- Extending module system to allow for overwriting/extending core modules using custom modules
- Status and Rich Presence support
## Installation

Install by cloning:

```bash
  git clone https://gitlab.com/Czaroslaw/cocobot-2.0.git
```
Go into the cloned repo folder and run `node .` to run the app. Upon the first launch, messages such as the following should appear:
```bash
  No config file detected. A new one has been created in /config/mainConfig.json.
```
```bash
  No settings file detected. A new one has been created in /config/settings.json.
```
The `mainConfig.json` file contains the following structure
```json
    "main": {
        "token": "",
        "derpiKey": ""
    }
```
The `token` value must be filled with the token corresponding for your bot application. It can be found in the settings of your application in the Discord Developer Portal: https://discord.com/developers/applications

The `settings.json` file will be filled with example settings, which are enough for the application to work:
```json
{
    "status": "",
    "prefix": {
        "key": "coco",
        "ignoreInputCapitalization": "true",
        "allowUseBotMention": "true"
    },
    "ownerID": ""
}
```

Don't forget to install `discord.js` using `npm`:
```bash
npm install discord.js
```

## Creating new modules

CocoBot 2.0 comes with a built-in module generating script. It will generate a basic structure for the module, which will be the bare requirement for the module to load properly.

### Usage:
```bash
npm run module create <name>
```
A structure like following will be generated:
```bash
example_module/
├─ src/
│  ├─ main.js
├─ module.json
├─ settings.json
``` 
The module loader will **always** look for the `<module_root>/src/main.js` file, as well as the `module.json` and `settings.json`.
The `main.js` must contain the code. Be it the entire code in one file, or `require` statements, is up to you and your needs.
