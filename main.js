require('./src/events/firstRun.js');

const { Client, GatewayIntentBits } = require('discord.js');
const { main: mainConfig } = require('./config/mainConfig.json');
global.settings = require('./config/settings.json');

global.appRoot = __dirname;
global.client = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildMembers,
    ],
});
require('./src/shared/eventHandlers.js');
require('./src/shared/moduleLoader.js');
global.client.login(mainConfig.token);